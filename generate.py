def read_prime_numbers(path, max_number):
    with open('./100000.txt') as file:
        lines = file.read().splitlines()

    numbers = []
    for line in lines[7:]:
        for token in line.split():
            n = int(token)
            if n <= max_number:
                numbers.append(n)
    return numbers


def main():
    max_number = 1_000_000
    numbers = set(read_prime_numbers('100000.txt', max_number))
    byte_count = max_number // 8 + (1 if max_number % 8 != 0 else 0)
    output_bytes = bytearray()
    for i in range(0, byte_count):
        b = 0
        for j in range(0, 8):
            if (i * 8 + j) in numbers:
                b |= 1 << j
        output_bytes.append(b)

    with open('prime_numbers.dat', 'wb') as file:
        file.write(output_bytes)


if __name__ == '__main__':
    main()

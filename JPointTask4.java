import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class JPointTask4 {
    static byte[] packed_prime_numbers;

    public static boolean isPrimeNumber(int n) {
        return ((packed_prime_numbers[n / 8] >>> (n % 8)) & 1) == 1;
    }

    public static void main(String[] args) {
        try {
            packed_prime_numbers = Files.readAllBytes(Paths.get("./prime_numbers.dat"));
        } catch (IOException e) {
            System.err.println("Failed to read prime numbers from file: " + e.getMessage());
            return;
        }

        var scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int number = scanner.nextInt();
            if (isPrimeNumber(number)) {
                System.out.println(number + " - is prime number");
            } else {
                System.out.println(number + " - is NOT prime number");
            }
        }
    }
}
